Cypress.Commands.add('getAuthorization', (user, passwd) => {
    cy.request({
        method: 'POST',
        url: '/login',
        body: {
            email: user,
            password: passwd
        }
    }).its('body.authorization').should('not.be.empty')
        .then(authorization => {
            return authorization

        })

})