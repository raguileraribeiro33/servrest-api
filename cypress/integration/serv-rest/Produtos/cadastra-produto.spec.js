///<reference types="cypress" />

describe('Realizar cadastro produto', () => {
    before(() => {
        cy.getAuthorization('sara@qa.com.br','test1234')
            .then(auth =>{
                authorization = auth
            })

    })

    beforeEach(() => {

    });

    it('Login com sucesso', () => {
        cy.getAuthorization('sara@qa.com.br', 'test1234')

            .then(authorization => {
                cy.request({
                    url: '/produtos',
                    method: 'POST',
                    headers: { Authorization: `Bearer ${authorization}` },
                    body: {
                        "nome": "Duke Nukem",
                        "preco": 22,
                        "descricao": "Jogo para console PS1",
                        "quantidade": 100

                    }
                }).as('response')
            })

        cy.get('@response').then(res => {
            expect(res.status).to.be.equal(201)
            expect(res.body).to.have.property('descricao')
            expect(res.body).to.have.property('nome', 'Duke Nukem')
        })
    });

});