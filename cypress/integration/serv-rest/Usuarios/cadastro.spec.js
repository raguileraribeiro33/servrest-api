///<reference types="cypress" />

describe('Cadastrar usuário', () => {
    before(() => {
        
    });
    beforeEach(() => {
        
    });

    it('Criar cadastro com sucesso', () => {
        cy.request({
            method: 'POST',
            url:'/usuarios',
            body:{
                "nome": "Sara Connor",
                "email": "sara@qa.com.br",
                "password": "test1234",
                "administrador": "true"
            }
        }).its('body._id').should('not.be.empty')
        
    });
});